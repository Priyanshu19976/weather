import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@Test
public class WeatherForecast {
  RemoteWebDriver driver;
  
	@Given("^Open the weather forecast website$")
	public void open_the_weather_forecast_website() throws MalformedURLException {
		ChromeDriverService options = null;
		//ChromeOptions options=new ChromeOptions();
		
		DesiredCapabilities cap=DesiredCapabilities.chrome();
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		String huburl ="http://192.168.225.208:4444/wd/hub";
		driver = new RemoteWebDriver(new URL(huburl),cap);
		driver.manage().window().maximize();
		driver.get("http://timeanddate.com/weather/");
	}

	@When("^Search the city$")
	public void search_the_city() {
		driver.findElement(By.xpath("//*[@id=\"sb_wc_q\"]")).sendKeys("Bengaluru");
	    driver.findElement(By.className("picker-city__button")).click();
	    driver.findElement(By.xpath("/html/body/div[1]/div[6]/section[1]/div/section[2]/div[1]/div/table/tbody/tr[1]/td[1]/a")).click();
	}
	
	@Then("^Fetch and report in html file$")
	public void fetch_and_report_in_html_file() throws IOException {
		try {
		FileWriter writer = new FileWriter("C:/Users/Priyanshu/Downloads/chromedriver_win32/Report.html", true);
	    writer.write("<html>");
	    writer.write("<body>");
	    WebElement htmltable=driver.findElement(By.id("wt-48"));
	    List<WebElement> rows=htmltable.findElements(By.tagName("tr"));
	     
	    for(int rnum=0;rnum<rows.size();rnum++)
	    {
	    	//System.out.println("");
	    	 writer.write("<br>");
	    	 writer.write("<br>");
	    List<WebElement> columns= rows.get(rnum).findElements(By.tagName("th"));
	    for(int cnum=0;cnum<columns.size();cnum++)
	    {
	    	writer.write(columns.get(cnum).getText() + " ");
	    //System.out.print(columns.get(cnum).getText() + " ");
	    
	    }
	    
	    List<WebElement> k= rows.get(rnum).findElements(By.tagName("td"));
	    for(int knum=0;knum<k.size();knum++)
	    {
	    	writer.write(k.get(knum).getText() + " ");
	    	//System.out.print(k.get(knum).getText() + " ");
	    
	    }
	    
	   //System.out.println("\n");
	    }
	    writer.write("</html>");
	    writer.write("</body>");
	   writer.close();
	
	}
	catch(Exception e){}
}
	}
