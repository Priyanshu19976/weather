package TestRunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue= {"classpath:"},
		plugin = {"pretty","html:src/target/cucumber","json:src/target/cucumber.json"})
public class Runner {

}